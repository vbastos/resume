# William Vieira Bastos - Resume

This [page](https://vbastos.gitlab.io/resume) is intended to provide an initial view of my personal and professional profile to interested parties.
The page was built using the [Bootstrap](http://getbootstrap.com/) thema [Resume](https://startbootstrap.com/template-overviews/resume/) created by [Start Bootstrap](http://startbootstrap.com/).



------------
###### Copyright 2013-2019 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-resume/blob/gh-pages/LICENSE) license.